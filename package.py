name = "liclipse"
version = "2.3.0"
authors = ["brainwy"]
description = "Lightweight editors, theming and usability improvements for Eclipse"
tools = ["liclipse"]
# requires = ["python"]
# help = "file://{root}/help.html"
uuid = "0a55a035-a844-49c0-b75f-c2174673da42"
def commands():
    env.PATH.append("$BD_SOFTS/liclipse/{version}")
